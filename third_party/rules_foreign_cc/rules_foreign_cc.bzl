load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "0.10.1"

def rules_foreign_cc():
    maybe(
        http_archive,
        name = "rules_foreign_cc",
        sha256 = "476303bd0f1b04cc311fc258f1708a5f6ef82d3091e53fd1977fa20383425a6a",
        strip_prefix = "rules_foreign_cc-{version}".format(version = _VERSION),
        url = "https://github.com/bazelbuild/rules_foreign_cc/releases/download/{version}/rules_foreign_cc-{version}.tar.gz".format(version = _VERSION)
    )
