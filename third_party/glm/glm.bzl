load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "0.9.9.7"

def glm_gtgen():
    """ External dependencies for glm version for gtgen """
    maybe(
        http_archive,
        name = "glm_gtgen",
        sha256 = "2ec9e33a80b548892af64fbd84a947f93f0e725423b1b7bec600f808057a8239",
        url = "https://github.com/g-truc/glm/archive/refs/tags/{version}.tar.gz".format(version = _VERSION),
        build_file = Label("//:third_party/glm/glm.BUILD"),
        strip_prefix = "glm-{version}".format(version = _VERSION),
    )
