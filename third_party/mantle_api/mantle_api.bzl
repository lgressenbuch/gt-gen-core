load("@gt_gen_core//third_party:upstream_remotes.bzl", "ECLIPSE_GITLAB")
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v2.1.0"

def mantle_api():
    maybe(
        http_archive,
        name = "mantle_api",
        url = ECLIPSE_GITLAB + "/openpass/mantle-api/-/archive/{tag}/mantle-api-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "47f96078d4bda8b5394aa11e8d11835affd825f164195d62028de92cce5032f9",
        strip_prefix = "mantle-api-{tag}".format(tag = _TAG),
        type = "tar.gz",
    )
