load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "1-24-0"

def asio():
    maybe(
        http_archive,
        name = "asio",
        sha256 = "cbcaaba0f66722787b1a7c33afe1befb3a012b5af3ad7da7ff0f6b8c9b7a8a5b",
        build_file = Label("//:third_party/asio/asio.BUILD"),
        url = "https://github.com/chriskohlhoff/asio/archive/refs/tags/asio-{version}.tar.gz".format(version = _VERSION),
        strip_prefix = "asio-asio-{version}".format(version = _VERSION),
    )
