load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "3.5.0"

def open_simulation_interface():
    maybe(
        http_archive,
        name = "open_simulation_interface",
        build_file = Label("//:third_party/open_simulation_interface/open_simulation_interface.BUILD"),
        url = "https://github.com//OpenSimulationInterface/open-simulation-interface/archive/refs/tags/v{version}.tar.gz".format(version = _VERSION),
        sha256 = "8555aa97a5dd248d043e311cbf851e9c3e5ba90d42c04988cc7c1785dfbe8474",
        strip_prefix = "open-simulation-interface-{version}".format(version = _VERSION),
        patch_cmds = [
            "set -o errexit",
            "set -o nounset",
            "set -o pipefail",
            # Remove the last 12 problematic lines from version template and save as normal proto file,
            # because `current_interface_version` can cause issues with plugins
            # See issue: https://github.com/OpenSimulationInterface/open-simulation-interface/issues/592
            "head -n -12 osi_version.proto.in > osi_version.proto",
            # Remove the `import "google/protobuf/descriptor.proto";` statement from osi_version.proto
            "sed -i 's/import \"google\\/protobuf\\/descriptor.proto\";//' osi_version.proto",
            # Upgrade to proto3
            "find . -name '*.proto' -print0 | xargs -0 sed -i '\
                s/syntax *= *\"proto2\";/syntax = \"proto3\";/; \
                s/^\\([ \t]*\\)optional /\\1/'",
        ],
    )
