From f5d56094f159c1751387a336cb79f89078ec0fcb Mon Sep 17 00:00:00 2001
From: Antony Polukhin <antoshkka@gmail.com>
Date: Sat, 23 Nov 2019 20:59:00 +0300
Subject: [PATCH] fix warnings and work with enum without UB (fixes #30)

---
 .../dll/detail/posix/shared_library_impl.hpp  | 27 ++++++++++---------
 .../detail/windows/shared_library_impl.hpp    | 19 ++++++-------
 include/boost/dll/library_info.hpp            |  2 +-
 3 files changed, 25 insertions(+), 23 deletions(-)

diff --git boost/dll/detail/posix/shared_library_impl.hpp boost/dll/detail/posix/shared_library_impl.hpp
index 6ecb38c3..b6444e3d 100644
--- boost/dll/detail/posix/shared_library_impl.hpp
+++ boost/dll/detail/posix/shared_library_impl.hpp
@@ -69,8 +69,9 @@ class shared_library_impl {
         return actual_path;
     }
 
-    void load(boost::dll::fs::path sl, load_mode::type mode, boost::dll::fs::error_code &ec) {
+    void load(boost::dll::fs::path sl, load_mode::type portable_mode, boost::dll::fs::error_code &ec) {
         typedef int native_mode_t;
+        native_mode_t native_mode = static_cast<native_mode_t>(portable_mode);
         unload();
 
         // Do not allow opening NULL paths. User must use program_location() instead
@@ -84,20 +85,20 @@ class shared_library_impl {
         }
 
         // Fixing modes
-        if (!(mode & load_mode::rtld_now)) {
-            mode |= load_mode::rtld_lazy;
+        if (!(native_mode & load_mode::rtld_now)) {
+            native_mode |= load_mode::rtld_lazy;
         }
 
-        if (!(mode & load_mode::rtld_global)) {
-            mode |= load_mode::rtld_local;
+        if (!(native_mode & load_mode::rtld_global)) {
+            native_mode |= load_mode::rtld_local;
         }
 
 #if BOOST_OS_LINUX || BOOST_OS_ANDROID
-        if (!sl.has_parent_path() && !(mode & load_mode::search_system_folders)) {
+        if (!sl.has_parent_path() && !(native_mode & load_mode::search_system_folders)) {
             sl = "." / sl;
         }
 #else
-        if (!sl.is_absolute() && !(mode & load_mode::search_system_folders)) {
+        if (!sl.is_absolute() && !(native_mode & load_mode::search_system_folders)) {
             boost::dll::fs::error_code current_path_ec;
             boost::dll::fs::path prog_loc = boost::dll::fs::current_path(current_path_ec);
             if (!current_path_ec) {
@@ -107,14 +108,14 @@ class shared_library_impl {
         }
 #endif
 
-        mode &= ~load_mode::search_system_folders;
+        native_mode = static_cast<unsigned>(native_mode) & ~static_cast<unsigned>(load_mode::search_system_folders);
 
         // Trying to open with appended decorations
-        if (!!(mode & load_mode::append_decorations)) {
-            mode &= ~load_mode::append_decorations;
+        if (!!(native_mode & load_mode::append_decorations)) {
+            native_mode = static_cast<unsigned>(native_mode) & ~static_cast<unsigned>(load_mode::append_decorations);
 
             boost::dll::fs::path actual_path = decorate(sl);
-            handle_ = dlopen(actual_path.c_str(), static_cast<native_mode_t>(mode));
+            handle_ = dlopen(actual_path.c_str(), native_mode);
             if (handle_) {
                 boost::dll::detail::reset_dlerror();
                 return;
@@ -131,7 +132,7 @@ class shared_library_impl {
         }
 
         // Opening by exactly specified path
-        handle_ = dlopen(sl.c_str(), static_cast<native_mode_t>(mode));
+        handle_ = dlopen(sl.c_str(), native_mode);
         if (handle_) {
             boost::dll::detail::reset_dlerror();
             return;
@@ -153,7 +154,7 @@ class shared_library_impl {
             // returned handle is for the main program.
             ec.clear();
             boost::dll::detail::reset_dlerror();
-            handle_ = dlopen(NULL, static_cast<native_mode_t>(mode));
+            handle_ = dlopen(NULL, native_mode);
             if (!handle_) {
                 ec = boost::dll::fs::make_error_code(
                     boost::dll::fs::errc::bad_file_descriptor
diff --git boost/dll/detail/windows/shared_library_impl.hpp boost/dll/detail/windows/shared_library_impl.hpp
index ca584fad..4df18b48 100644
--- boost/dll/detail/windows/shared_library_impl.hpp
+++ boost/dll/detail/windows/shared_library_impl.hpp
@@ -56,11 +56,12 @@ class shared_library_impl {
         return actual_path;
     }
 
-    void load(boost::dll::fs::path sl, load_mode::type mode, boost::dll::fs::error_code &ec) {
+    void load(boost::dll::fs::path sl, load_mode::type portable_mode, boost::dll::fs::error_code &ec) {
         typedef boost::winapi::DWORD_ native_mode_t;
+        native_mode_t native_mode = static_cast<native_mode_t>(portable_mode);
         unload();
 
-        if (!sl.is_absolute() && !(mode & load_mode::search_system_folders)) {
+        if (!sl.is_absolute() && !(native_mode & load_mode::search_system_folders)) {
             boost::dll::fs::error_code current_path_ec;
             boost::dll::fs::path prog_loc = boost::dll::fs::current_path(current_path_ec);
 
@@ -69,13 +70,13 @@ class shared_library_impl {
                 sl.swap(prog_loc);
             }
         }
-        mode &= ~load_mode::search_system_folders;
+        native_mode = static_cast<unsigned>(native_mode) & ~static_cast<unsigned>(load_mode::search_system_folders);
 
         // Trying to open with appended decorations
-        if (!!(mode & load_mode::append_decorations)) {
-            mode &= ~load_mode::append_decorations;
+        if (!!(native_mode & load_mode::append_decorations)) {
+            native_mode = static_cast<unsigned>(native_mode) & ~static_cast<unsigned>(load_mode::append_decorations);
 
-            if (load_impl(decorate(sl), static_cast<native_mode_t>(mode), ec)) {
+            if (load_impl(decorate(sl), native_mode, ec)) {
                 return;
             }
 
@@ -85,7 +86,7 @@ class shared_library_impl {
                 ? sl.parent_path() / L"lib"
                 : L"lib"
             ).native() + sl.filename().native() + suffix().native();
-            if (load_impl(mingw_load_path, static_cast<native_mode_t>(mode), ec)) {
+            if (load_impl(mingw_load_path, native_mode, ec)) {
                 return;
             }
         }
@@ -98,9 +99,9 @@ class shared_library_impl {
         // we have some path. So we do not check for path, only for extension. We can not be sure that
         // such behavior remain across all platforms, so we add L"." by hand.
         if (sl.has_extension()) {
-            handle_ = boost::winapi::LoadLibraryExW(sl.c_str(), 0, static_cast<native_mode_t>(mode));
+            handle_ = boost::winapi::LoadLibraryExW(sl.c_str(), 0, native_mode);
         } else {
-            handle_ = boost::winapi::LoadLibraryExW((sl.native() + L".").c_str(), 0, static_cast<native_mode_t>(mode));
+            handle_ = boost::winapi::LoadLibraryExW((sl.native() + L".").c_str(), 0, native_mode);
         }
 
         // LoadLibraryExW method is capable of self loading from program_location() path. No special actions
