/*******************************************************************************
 * Copyright (c) 2019-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MAPTYPECONVERSIONS_H
#define GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MAPTYPECONVERSIONS_H

#include "Core/Environment/Map/GtGenMap/lane.h"
#include "Core/Environment/Map/GtGenMap/lane_boundary.h"
#include "Core/Environment/Map/GtGenMap/lane_group.h"
#include "Core/Service/Osi/stationary_object_types.h"
#include "gtgen_map.pb.h"

#include <MantleAPI/Traffic/entity_properties.h>

namespace gtgen::core::communication
{

inline messages::map::LaneGroupType GtGenToProtoLaneGroupType(environment::map::LaneGroup::Type gtgen_lane_group_type)
{
    using gtgen_type = environment::map::LaneGroup::Type;
    using proto_type = messages::map::LaneGroupType;

    switch (gtgen_lane_group_type)
    {
        case gtgen_type::kOther:
        {
            return proto_type::LANE_GROUP_TYPE_OTHER;
        }
        case gtgen_type::kOneWay:
        {
            return proto_type::LANE_GROUP_TYPE_ONE_WAY;
        }
        case gtgen_type::kJunction:
        {
            return proto_type::LANE_GROUP_TYPE_JUNCTION;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::LANE_GROUP_TYPE_UNKNOWN;
        }
    }
}

inline messages::map::LaneBoundaryColor GtGenToProtoBoundaryColor(
    environment::map::LaneBoundary::Color gtgen_boundary_color)
{
    using gtgen_type = environment::map::LaneBoundary::Color;
    using proto_type = messages::map::LaneBoundaryColor;

    switch (gtgen_boundary_color)
    {
        case gtgen_type::kOther:
        {
            return proto_type::BOUNDARY_COLOR_OTHER;
        }
        case gtgen_type::kNone:
        {
            return proto_type::BOUNDARY_COLOR_NONE;
        }
        case gtgen_type::kWhite:
        {
            return proto_type::BOUNDARY_COLOR_WHITE;
        }
        case gtgen_type::kYellow:
        {
            return proto_type::BOUNDARY_COLOR_YELLOW;
        }
        case gtgen_type::kRed:
        {
            return proto_type::BOUNDARY_COLOR_RED;
        }
        case gtgen_type::kBlue:
        {
            return proto_type::BOUNDARY_COLOR_BLUE;
        }
        case gtgen_type::kGreen:
        {
            return proto_type::BOUNDARY_COLOR_GREEN;
        }
        case gtgen_type::kViolet:
        {
            return proto_type::BOUNDARY_COLOR_VIOLET;
        }
        case gtgen_type::kLightGray:
        {
            return proto_type::BOUNDARY_COLOR_LIGHTGRAY;
        }
        case gtgen_type::kGray:
        {
            return proto_type::BOUNDARY_COLOR_GRAY;
        }
        case gtgen_type::kDarkGray:
        {
            return proto_type::BOUNDARY_COLOR_DARKGRAY;
        }
        case gtgen_type::kBlack:
        {
            return proto_type::BOUNDARY_COLOR_BLACK;
        }
        case gtgen_type::kCyan:
        {
            return proto_type::BOUNDARY_COLOR_CYAN;
        }
        case gtgen_type::kOrange:
        {
            return proto_type::BOUNDARY_COLOR_ORANGE;
        }
        case gtgen_type::kStandard:
        {
            return proto_type::BOUNDARY_COLOR_STANDARD;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::BOUNDARY_COLOR_UNKNOWN;
        }
    }
}

inline messages::map::LaneBoundaryType GtGenToProtoBoundaryType(
    environment::map::LaneBoundary::Type gtgen_boundary_type)
{
    using gtgen_type = environment::map::LaneBoundary::Type;
    using proto_type = messages::map::LaneBoundaryType;

    switch (gtgen_boundary_type)
    {
        case gtgen_type::kOther:
        {
            return proto_type::BOUNDARY_TYPE_OTHER;
        }
        case gtgen_type::kNoLine:
        {
            return proto_type::BOUNDARY_TYPE_NOLINE;
        }
        case gtgen_type::kSolidLine:
        {
            return proto_type::BOUNDARY_TYPE_SOLIDLINE;
        }
        case gtgen_type::kDashedLine:
        {
            return proto_type::BOUNDARY_TYPE_DASHEDLINE;
        }
        case gtgen_type::kBottsDots:
        {
            return proto_type::BOUNDARY_TYPE_BOTTSDOTS;
        }
        case gtgen_type::kRoadEdge:
        {
            return proto_type::BOUNDARY_TYPE_ROADEDGE;
        }
        case gtgen_type::kSnowEdge:
        {
            return proto_type::BOUNDARY_TYPE_SNOWEDGE;
        }
        case gtgen_type::kGrassEdge:
        {
            return proto_type::BOUNDARY_TYPE_GRASSEDGE;
        }
        case gtgen_type::kGravelEdge:
        {
            return proto_type::BOUNDARY_TYPE_GRAVELEDGE;
        }
        case gtgen_type::kSoilEdge:
        {
            return proto_type::BOUNDARY_TYPE_SOILEDGE;
        }
        case gtgen_type::kGuardRail:
        {
            return proto_type::BOUNDARY_TYPE_GUARDRAIL;
        }
        case gtgen_type::kCurb:
        {
            return proto_type::BOUNDARY_TYPE_CURB;
        }
        case gtgen_type::kStructure:
        {
            return proto_type::BOUNDARY_TYPE_STRUCTURE;
        }
        case gtgen_type::kBarrier:
        {
            return proto_type::BOUNDARY_TYPE_BARRIER;
        }
        case gtgen_type::kUnknown:
        default:
        {
            return proto_type::BOUNDARY_TYPE_UNKNOWN;
        }
    }
}

inline messages::map::LaneType GtGenToProtoLaneType(environment::map::LaneFlags lane_flags)
{
    if (lane_flags.IsDrivable())
    {
        return messages::map::LaneType::LANE_TYPE_DRIVING;
    }
    return messages::map::LaneType::LANE_TYPE_NON_DRIVING;
}

inline messages::map::RoadObjectType GtGenToProtoRoadObjectType(mantle_api::StaticObjectType gtgen_road_object_type)
{
    using gtgen_type = mantle_api::StaticObjectType;
    using proto_type = messages::map::RoadObjectType;

    switch (gtgen_road_object_type)
    {
        case gtgen_type::kOther:
        {
            return proto_type::ROAD_OBJECT_TYPE_OTHER;
        }
        case gtgen_type::kBridge:
        {
            return proto_type::ROAD_OBJECT_TYPE_BRIDGE;
        }
        case gtgen_type::kBuilding:
        {
            return proto_type::ROAD_OBJECT_TYPE_BUILDING;
        }
        case gtgen_type::kPole:
        {
            return proto_type::ROAD_OBJECT_TYPE_POLE;
        }
        case gtgen_type::kPylon:
        {
            return proto_type::ROAD_OBJECT_TYPE_PYLON;
        }
        case gtgen_type::kDelineator:
        {
            return proto_type::ROAD_OBJECT_TYPE_DELINEATOR;
        }
        case gtgen_type::kTree:
        {
            return proto_type::ROAD_OBJECT_TYPE_TREE;
        }
        case gtgen_type::kBarrier:
        {
            return proto_type::ROAD_OBJECT_TYPE_BARRIER;
        }
        case gtgen_type::kVegetation:
        {
            return proto_type::ROAD_OBJECT_TYPE_VEGETATION;
        }
        case gtgen_type::kCurbstone:
        {
            return proto_type::ROAD_OBJECT_TYPE_CURBSTONE;
        }
        case gtgen_type::kWall:
        {
            return proto_type::ROAD_OBJECT_TYPE_WALL;
        }
        case gtgen_type::kVerticalStructure:
        {
            return proto_type::ROAD_OBJECT_TYPE_VERTICALSTRUCTURE;
        }
        case gtgen_type::kRectangularStructure:
        {
            return proto_type::ROAD_OBJECT_TYPE_RECTANGULARSTRUCTURE;
        }
        case gtgen_type::kOverheadStructure:
        {
            return proto_type::ROAD_OBJECT_TYPE_OVERHEADSTRUCTURE;
        }
        case gtgen_type::kReflectiveStructure:
        {
            return proto_type::ROAD_OBJECT_TYPE_REFLECTIVESTRUCTURE;
        }
        case gtgen_type::kConstructionSiteElement:
        {
            return proto_type::ROAD_OBJECT_TYPE_CONSTRUCTIONSITEELEMENT;
        }
        case gtgen_type::kInvalid:
        default:
        {
            return proto_type::ROAD_OBJECT_TYPE_UNKNOWN;
        }
    }
}

}  // namespace gtgen::core::communication

#endif  // GTGEN_CORE_COMMUNICATION_SERIALIZATION_INTERNAL_MAPTYPECONVERSIONS_H
