/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_LANEGROUPUTILITY_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_LANEGROUPUTILITY_H

#include "Core/Environment/Map/GtGenMap/lane_group.h"

#include <fmt/format.h>

#include <string>

namespace gtgen::core::environment::map
{

std::string ToVerboseString(const LaneGroup& lane_group)
{
    return fmt::format("LaneGroup:\nID: {}\nLaneGroup::Type: {}\nLane-Count: {}\nBoundary-Count: {}",
                       lane_group.id,
                       lane_group.type,
                       lane_group.lane_ids.size(),
                       lane_group.lane_boundary_ids.size());
}

std::string LaneGroupTypeToString(LaneGroup::Type type)
{
    switch (type)
    {
        case LaneGroup::Type::kUnknown:
        {
            return "Unknown";
        }
        case LaneGroup::Type::kOther:
        {
            return "Other";
        }
        case LaneGroup::Type::kOneWay:
        {
            return "OneWay";
        }
        case LaneGroup::Type::kJunction:
        {
            return "Junction";
        }
        default:
        {
            return "Unknown";
        }
    }
}

}  // namespace gtgen::core::environment::map

template <>
struct fmt::formatter<gtgen::core::environment::map::LaneGroup::Type>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::environment::map::LaneGroup::Type& type, FormatContext& ctx)
    {
        return format_to(ctx.out(), "{}", gtgen::core::environment::map::LaneGroupTypeToString(type));
    }
};

template <>
struct fmt::formatter<gtgen::core::environment::map::LaneGroup>
{
    template <typename ParseContext>
    constexpr auto parse(ParseContext& ctx)  // NOLINT(readability-identifier-naming)
    {
        return ctx.begin();
    }

    template <typename FormatContext>
    // NOLINTNEXTLINE(readability-identifier-naming)
    auto format(const gtgen::core::environment::map::LaneGroup& lane_group, FormatContext& ctx)
    {
        return format_to(ctx.out(), "LaneGroup {}", lane_group.id);
    }
};

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GTGENMAP_LANEGROUPUTILITY_H
