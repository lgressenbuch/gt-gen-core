/*******************************************************************************
 * Copyright (c) 2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Map/MapApiConverter/Internal/mapapi_to_gtgenmap_converter_impl.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Environment/Map/MapApiConverter/Internal/lane_boundary_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/lane_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/lane_group_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/roadmarking_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/traffic_light_converter.h"
#include "Core/Environment/Map/MapApiConverter/Internal/traffic_sign_converter.h"

namespace gtgen::core::environment::map
{

MapApiToGtGenMapConverterImpl::MapApiToGtGenMapConverterImpl(service::utility::UniqueIdProvider& id_provider,
                                                             const map_api::Map& data,
                                                             GtGenMap& gtgen_map)
    : unique_id_provider_{id_provider}, data_{data}, gtgen_map_{gtgen_map}
{
}

MapApiToGtGenMapConverterImpl::~MapApiToGtGenMapConverterImpl() = default;

void MapApiToGtGenMapConverterImpl::Convert()
{
    if (!(gtgen_map_.IsOpenDrive() || gtgen_map_.IsNDS()))
    {
        LogAndThrow(EnvironmentException("GtGen map source type is not set!"));
    }

    gtgen_map_.path = data_.map_reference;
    gtgen_map_.projection_string = data_.projection_string;
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId> lane_to_lane_group_id_map{};
    std::unordered_map<mantle_api::UniqueId, mantle_api::UniqueId> lane_boundary_to_lane_group_id_map{};

    // convert lane groups
    for (const auto& lane_group : data_.lane_groups)
    {
        gtgen_map_.AddLaneGroup(CreateLaneGroup(lane_group->id, lane_group->type));
        AppendLaneAndLaneBoundaryToLaneGroupIdMap(
            *lane_group, lane_to_lane_group_id_map, lane_boundary_to_lane_group_id_map);
    }

    // convert lane_boundaries
    for (const auto& lane_boundary : data_.lane_boundaries)
    {
        gtgen_map_.AddLaneBoundary(lane_boundary_to_lane_group_id_map[lane_boundary->id],
                                   ConvertLaneBoundary(*lane_boundary));
    }

    // convert lanes
    for (const auto& lane : data_.lanes)
    {
        gtgen_map_.AddLane(lane_to_lane_group_id_map[lane->id], ConvertLane(*lane));
    }

    // convert traffic signs
    for (const auto& traffic_sign : data_.traffic_signs)
    {
        FillTrafficSign(*traffic_sign, gtgen_map_);
    }

    // convert traffic lights
    for (const auto& traffic_light : data_.traffic_lights)
    {
        gtgen_map_.traffic_lights.emplace_back(ConvertTrafficLight(*traffic_light));
    }

    // convert ground signs
    for (const auto& road_marking : data_.road_markings)
    {
        auto ground_sign = std::make_shared<GroundSign>(ConvertRoadMarking(*road_marking));
        gtgen_map_.traffic_signs.emplace_back(ground_sign);
    }

    ///@todo  Add conversion of road objects
}

std::map<mantle_api::UniqueId, mantle_api::UniqueId> MapApiToGtGenMapConverterImpl::GetNativeToGtGenTrafficLightIdMap()
    const
{
    return {};
}

}  // namespace gtgen::core::environment::map
