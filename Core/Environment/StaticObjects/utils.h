/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_UTILS_H
#define GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_UTILS_H

#include <optional>
#include <string>

namespace gtgen::core::environment::static_objects
{

std::string GetTypeIdentifier(const std::string& identifier);
std::optional<std::string> GetSignalSubType(const std::string& identifier);

}  // namespace gtgen::core::environment::static_objects

#endif  // GTGEN_CORE_ENVIRONMENT_STATICOBJECTS_UTILS_H
