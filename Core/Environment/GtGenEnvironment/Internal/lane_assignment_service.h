/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_LANEASSIGNMENTSERVICE_H
#define GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_LANEASSIGNMENTSERVICE_H

#include "Core/Environment/Entities/Internal/base_entity.h"
#include "Core/Environment/GtGenEnvironment/Internal/active_controller_repository.h"
#include "Core/Environment/Map/LaneLocationProvider/lane_location_provider.h"

#include <MantleAPI/Traffic/i_entity_repository.h>

namespace gtgen::core::environment::api
{

class LaneAssignmentService
{
  public:
    LaneAssignmentService(mantle_api::IEntityRepository& entity_repository,
                          const ActiveControllerRepository& active_controller_repository);

    void Step();

    void SetLaneLocationProvider(const map::LaneLocationProvider* lane_location_provider);
    void SetIsEntityAllowedToLeaveLane(bool is_entity_allowed_to_leave_lane);

  private:
    std::vector<mantle_api::LaneId> GetScenarioAssignedLaneForEntity(
        gtgen::core::environment::entities::BaseEntity* entity) const;

    const map::LaneLocationProvider* lane_location_provider_;
    mantle_api::IEntityRepository& entity_repository_;
    const ActiveControllerRepository& active_controller_repository_;
    bool is_entity_allowed_to_leave_lane_{false};
};

}  // namespace gtgen::core::environment::api

#endif  // GTGEN_CORE_ENVIRONMENT_ASTASENVIRONMENT_INTERNAL_LANEASSIGNMENTSERVICE_H
