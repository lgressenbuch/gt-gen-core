/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/Host/vehicle_model_converter.h"

#include "Core/Environment/Exception/exception.h"
#include "Core/Service/GroundTruthConversions/proto_to_mantle.h"
#include "Core/Service/Logging/logging.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/Traffic/entity_properties.h>

namespace gtgen::core::environment::host
{

mantle_api::ExternalControlState ConvertADFunctionStateToExternalControlState(const osi3::TrafficUpdate& traffic_update)
{
    if (traffic_update.internal_state_size() > 0)
    {
        if (traffic_update.internal_state(0).vehicle_automated_driving_function_size() > 0)
        {
            if (traffic_update.internal_state(0).vehicle_automated_driving_function(0).state() ==
                osi3::HostVehicleData_VehicleAutomatedDrivingFunction_State::
                    HostVehicleData_VehicleAutomatedDrivingFunction_State_STATE_ACTIVE)
            {
                return mantle_api::ExternalControlState::kFull;
            }
        }
    }

    return mantle_api::ExternalControlState::kOff;
}

VehicleModelOut ConvertProtoToVehicleModelOut(const osi3::TrafficUpdate& proto_traffic_update)
{
    VehicleModelOut gtgen_vehicle_model_out{};

    if (proto_traffic_update.update_size() < 1)
    {
        environment::LogAndThrow(
            environment::EnvironmentException("Extraction of Object in the Traffic Update representing the Host "
                                              "Vehicle is not possible. Number of Moving Objects found: {}",
                                              proto_traffic_update.update_size()));
    }

    gtgen_vehicle_model_out.time_stamp = service::gt_conversion::ToTime(proto_traffic_update.timestamp());
    gtgen_vehicle_model_out.had_control_state = ConvertADFunctionStateToExternalControlState(proto_traffic_update);

    const auto& proto_host = proto_traffic_update.update(0);

    gtgen_vehicle_model_out.indicator_state =
        service::gt_conversion::ToIndicatorState(proto_host.vehicle_classification().light_state().indicator_state());

    const auto& base = proto_host.base();
    const auto& position = base.position();
    gtgen_vehicle_model_out.position = service::gt_conversion::ToVec3Length(position);

    const auto& velocity = base.velocity();
    gtgen_vehicle_model_out.velocity = service::gt_conversion::ToVec3Velocity(velocity);

    const auto& acceleration = base.acceleration();
    gtgen_vehicle_model_out.acceleration = service::gt_conversion::ToVec3Acceleration(acceleration);

    const auto& orientation = base.orientation();
    gtgen_vehicle_model_out.orientation = service::gt_conversion::ToOrientation3(orientation);

    const auto& orientation_rate = base.orientation_rate();
    gtgen_vehicle_model_out.orientation_rate = service::gt_conversion::ToOrientation3Rate(orientation_rate);

    return gtgen_vehicle_model_out;
}

}  // namespace gtgen::core::environment::host
