/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATICPROTOGROUNDTRUTHBUILDER_H
#define GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATICPROTOGROUNDTRUTHBUILDER_H

#include "Core/Environment/Chunking/world_chunk.h"
#include "Core/Environment/Map/GtGenMap/gtgen_map.h"
#include "osi_groundtruth.pb.h"

#include <list>
#include <unordered_set>

namespace gtgen::core::environment::proto_groundtruth
{

class StaticProtoGroundTruthBuilder
{
  public:
    explicit StaticProtoGroundTruthBuilder(const environment::map::GtGenMap& gtgen_map);

    /// @brief Fills provided ground truth object with static ground truth
    /// @param ground_truth Pointer that should be enriched with static info
    void FillStaticGroundTruth(const environment::chunking::WorldChunks& world_chunks,
                               osi3::GroundTruth& proto_ground_truth);
    void AddEntityIdToIgnoreList(mantle_api::UniqueId entity_id);
    const std::list<mantle_api::UniqueId>& GetIgnoreList() const { return entities_to_ignore_; }

  private:
    using GtGenLane = environment::map::Lane;
    using GtGenLaneBoundary = environment::map::LaneBoundary;
    using ProtoLane = osi3::Lane;
    using ProtoLaneBoundary = osi3::LaneBoundary;

    void FillLanesAndBoundaries(const environment::chunking::WorldChunks& world_chunks);
    void FillLanes(const environment::map::LaneGroup* lane_group,
                   std::unordered_set<mantle_api::UniqueId>& existing_lane_ids);
    void FillLaneBoundaries(const environment::map::LaneGroup* lane_group);
    void FillCenterLine(const GtGenLane& gtgen_lane, ProtoLane& gt_lane) const;
    void AddLaneBoundaries(const GtGenLane& gtgen_lane, ProtoLane& gt_lane) const;
    void FillLaneBoundary(const GtGenLaneBoundary& gtgen_boundary, ProtoLaneBoundary& osi_lane_boundary) const;

    void FillStationaryObjects(const environment::chunking::WorldChunks& world_chunks);
    void FillRoadObjects(const environment::chunking::WorldChunks& world_chunks);
    void FillTrafficLights(const environment::chunking::WorldChunks& world_chunks);
    void FillTrafficSigns(const environment::chunking::WorldChunks& world_chunks);
    void AddStaticEntities(const environment::chunking::WorldChunks& world_chunks);
    void AddEntityToGroundTruth(const mantle_api::IEntity* entity);

    const environment::map::GtGenMap& gtgen_map_;

    osi3::GroundTruth* proto_ground_truth_ptr_{nullptr};

    std::list<mantle_api::UniqueId> entities_to_ignore_{};
};
}  // namespace gtgen::core::environment::proto_groundtruth

#endif  // GTGEN_CORE_ENVIRONMENT_GROUNDTRUTH_INTERNAL_STATIC_STATICPROTOGROUNDTRUTHBUILDER_H
