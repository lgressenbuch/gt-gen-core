/*******************************************************************************
 * Copyright (c) 2019-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2019-2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/static_proto_ground_truth_builder.h"

#include "Core/Environment/GroundTruth/Internal/Static/lane_flags_converter.h"
#include "Core/Environment/GroundTruth/Internal/Static/lane_relation_ground_truth_builder.h"
#include "Core/Environment/GroundTruth/Internal/Static/stationary_object_proto_converter.h"
#include "Core/Environment/GroundTruth/Internal/Static/traffic_light_proto_converter.h"
#include "Core/Environment/GroundTruth/Internal/Static/traffic_sign_proto_converter.h"
#include "Core/Service/GroundTruthConversions/mantle_to_proto.h"
#include "Core/Service/MantleApiExtension/static_object_properties.h"
#include "Core/Service/Profiling/profiling.h"
#include "Core/Service/Utility/algorithm_utils.h"

namespace gtgen::core::environment::proto_groundtruth
{

StaticProtoGroundTruthBuilder::StaticProtoGroundTruthBuilder(const environment::map::GtGenMap& gtgen_map)
    : gtgen_map_{gtgen_map}
{
}

void StaticProtoGroundTruthBuilder::AddEntityIdToIgnoreList(mantle_api::UniqueId entity_id)
{
    if (!service::utility::Contains(entities_to_ignore_, entity_id))
    {
        entities_to_ignore_.push_back(entity_id);
    }
}

void StaticProtoGroundTruthBuilder::FillStaticGroundTruth(const environment::chunking::WorldChunks& world_chunks,
                                                          osi3::GroundTruth& proto_ground_truth)
{
    GTGEN_PROFILE_SCOPE

    proto_ground_truth_ptr_ = &proto_ground_truth;

    FillLanesAndBoundaries(world_chunks);
    FillStationaryObjects(world_chunks);
}

void StaticProtoGroundTruthBuilder::FillLanesAndBoundaries(const environment::chunking::WorldChunks& world_chunks)
{
    GTGEN_PROFILE_SCOPE

    // quick-lookup for existing lanes when filling the lane relations
    std::unordered_set<mantle_api::UniqueId> existing_lane_ids;
    for (const auto& chunk : world_chunks)
    {
        for (const auto* lane_group : chunk.lane_groups)
        {
            for (const auto& lane_id : lane_group->lane_ids)
            {
                existing_lane_ids.insert(lane_id);
            }
        }
    }

    std::unordered_set<const environment::map::LaneGroup*> added_lanes_groups;
    for (const auto& chunk : world_chunks)
    {
        for (const auto* lane_group : chunk.lane_groups)
        {
            if (!added_lanes_groups.emplace(lane_group).second)
            {
                continue;
            }

            GTGEN_PROFILE_SCOPE_NAMED("FillLaneGroup")

            FillLanes(lane_group, existing_lane_ids);
            FillLaneBoundaries(lane_group);
        }
    }
}

void StaticProtoGroundTruthBuilder::FillLanes(const environment::map::LaneGroup* lane_group,
                                              std::unordered_set<mantle_api::UniqueId>& existing_lane_ids)
{
    for (const auto& lane_id : lane_group->lane_ids)
    {
        GTGEN_PROFILE_SCOPE_NAMED("FillLane")

        auto proto_lane = proto_ground_truth_ptr_->add_lane();
        const auto& gtgen_lane = gtgen_map_.GetLane(lane_id);

        proto_lane->mutable_id()->set_value(gtgen_lane.id);
        AddLaneFlags(gtgen_lane, *proto_lane);

        LaneRelationGroundTruthBuilder builder(existing_lane_ids);
        builder.AddLanePairings(gtgen_lane, *proto_lane);
        builder.AddAdjacentLaneIds(gtgen_lane, *proto_lane);
        AddLaneBoundaries(gtgen_lane, *proto_lane);
        FillCenterLine(gtgen_lane, *proto_lane);
    }
}

void StaticProtoGroundTruthBuilder::FillLaneBoundaries(const environment::map::LaneGroup* lane_group)
{
    for (const auto& boundary_id : lane_group->lane_boundary_ids)
    {
        GTGEN_PROFILE_SCOPE_NAMED("FillBoundary")

        auto proto_boundary = proto_ground_truth_ptr_->add_lane_boundary();
        const auto& gtgen_boundary = gtgen_map_.GetLaneBoundary(boundary_id);
        FillLaneBoundary(gtgen_boundary, *proto_boundary);
    }
}

void StaticProtoGroundTruthBuilder::FillLaneBoundary(const GtGenLaneBoundary& gtgen_boundary,
                                                     ProtoLaneBoundary& osi_lane_boundary) const
{
    GTGEN_PROFILE_SCOPE

    osi_lane_boundary.mutable_classification()->set_color(
        static_cast<osi3::LaneBoundary::Classification::Color>(gtgen_boundary.color));
    osi_lane_boundary.mutable_classification()->set_type(
        static_cast<osi3::LaneBoundary::Classification::Type>(gtgen_boundary.type));

    osi_lane_boundary.mutable_id()->set_value(gtgen_boundary.id);

    auto proto_lane_boundary_line = osi_lane_boundary.mutable_boundary_line();
    for (const auto& point : gtgen_boundary.points)
    {
        auto proto_lane_boundary_line_point = proto_lane_boundary_line->Add();
        service::gt_conversion::FillProtoObject(point.position, proto_lane_boundary_line_point->mutable_position());
        proto_lane_boundary_line_point->set_height(point.height);
        proto_lane_boundary_line_point->set_width(point.width);
    }
}

void StaticProtoGroundTruthBuilder::FillCenterLine(const GtGenLane& gtgen_lane, ProtoLane& gt_lane) const
{
    GTGEN_PROFILE_SCOPE
    auto proto_center_line = gt_lane.mutable_classification()->mutable_centerline();

    for (const auto& point : gtgen_lane.center_line)
    {
        auto proto_center_line_point = proto_center_line->Add();
        service::gt_conversion::FillProtoObject(point, proto_center_line_point);
    }
}

void StaticProtoGroundTruthBuilder::AddLaneBoundaries(const GtGenLane& gtgen_lane, ProtoLane& gt_lane) const
{
    GTGEN_PROFILE_SCOPE

    for (const auto& left_lane_boundary : gtgen_lane.left_lane_boundaries)
    {
        gt_lane.mutable_classification()->mutable_left_lane_boundary_id()->Add()->set_value(left_lane_boundary);
    }

    for (const auto& right_lane_boundary : gtgen_lane.right_lane_boundaries)
    {
        gt_lane.mutable_classification()->mutable_right_lane_boundary_id()->Add()->set_value(right_lane_boundary);
    }
}

void StaticProtoGroundTruthBuilder::AddEntityToGroundTruth(const mantle_api::IEntity* entity)
{
    if (dynamic_cast<mantle_ext::TrafficSignProperties*>(entity->GetProperties()) != nullptr)
    {
        AddTrafficSignEntityToGroundTruth(entity, *proto_ground_truth_ptr_);
    }
    else if (dynamic_cast<mantle_ext::TrafficLightProperties*>(entity->GetProperties()) != nullptr)
    {
        AddTrafficLightEntityToGroundTruth(entity, *proto_ground_truth_ptr_);
    }
    else if (dynamic_cast<mantle_ext::RoadMarkingProperties*>(entity->GetProperties()) != nullptr)
    {
        AddRoadMarkingEntityToGroundTruth(entity, *proto_ground_truth_ptr_);
    }
    else
    {
        if (entity->GetVisibility().traffic)
        {
            AddStationaryEntityToGroundTruth(entity, *proto_ground_truth_ptr_);
        }
    }
}

void StaticProtoGroundTruthBuilder::FillStationaryObjects(const environment::chunking::WorldChunks& world_chunks)
{
    GTGEN_PROFILE_SCOPE

    // TODO: remove when gtgen map has for sure no road objects left, but only IEntities

    FillRoadObjects(world_chunks);
    FillTrafficSigns(world_chunks);
    FillTrafficLights(world_chunks);
    AddStaticEntities(world_chunks);
}

void StaticProtoGroundTruthBuilder::FillRoadObjects(const environment::chunking::WorldChunks& world_chunks)
{
    GTGEN_PROFILE_SCOPE

    std::unordered_set<const environment::map::RoadObject*> added_road_objects;
    for (const auto& chunk : world_chunks)
    {
        for (const auto* gtgen_road_object : chunk.road_objects)
        {
            if (added_road_objects.emplace(gtgen_road_object).second &&
                !service::utility::Contains(entities_to_ignore_, gtgen_road_object->id))
            {
                FillProtoGroundTruthStationaryObject(*gtgen_road_object, *proto_ground_truth_ptr_);
            }
        }
    }
}

void StaticProtoGroundTruthBuilder::FillTrafficSigns(const environment::chunking::WorldChunks& world_chunks)
{
    GTGEN_PROFILE_SCOPE

    std::unordered_set<const environment::map::TrafficSign*> added_traffic_signs;

    for (const auto& chunk : world_chunks)
    {
        for (const auto* gtgen_traffic_sign : chunk.traffic_signs)
        {
            if (added_traffic_signs.emplace(gtgen_traffic_sign).second)
            {
                if (const auto* gtgen_road_marking = dynamic_cast<const map::GroundSign*>(gtgen_traffic_sign))
                {
                    FillProtoGroundTruthRoadMarking(gtgen_road_marking, *proto_ground_truth_ptr_);
                }
                else
                {
                    FillProtoGroundTruthTrafficSign(gtgen_traffic_sign, *proto_ground_truth_ptr_);
                }
            }
        }
    }
}

void StaticProtoGroundTruthBuilder::FillTrafficLights(const environment::chunking::WorldChunks& world_chunks)
{
    GTGEN_PROFILE_SCOPE

    std::unordered_set<const environment::map::TrafficLight*> added_traffic_lights;
    for (const auto& chunk : world_chunks)
    {
        for (const environment::map::TrafficLight* gtgen_traffic_light : chunk.traffic_lights)
        {
            if (added_traffic_lights.emplace(gtgen_traffic_light).second)
            {
                FillProtoGroundTruthTrafficLight(gtgen_traffic_light, *proto_ground_truth_ptr_);
            }
        }
    }
}

void StaticProtoGroundTruthBuilder::AddStaticEntities(const environment::chunking::WorldChunks& world_chunks)
{
    GTGEN_PROFILE_SCOPE

    std::unordered_set<const mantle_api::IEntity*> added_static_entities;
    for (const auto& chunk : world_chunks)
    {
        for (const auto* entity : chunk.entities)
        {
            if (entity->GetProperties()->type == mantle_api::EntityType::kStatic)
            {
                if (added_static_entities.emplace(entity).second &&
                    !service::utility::Contains(entities_to_ignore_, entity->GetUniqueId()))
                {
                    AddEntityToGroundTruth(entity);
                }
            }
        }
    }
}

}  // namespace gtgen::core::environment::proto_groundtruth
